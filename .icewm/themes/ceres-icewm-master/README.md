Very simple and color neutral style for IceWM

[![Preview](https://gitlab.com/sixsixfive/ceres-icewm/raw/master/.preview.small.png)](https://gitlab.com/sixsixfive/ceres-icewm/raw/master/.preview.png)


### Features:

* [IceWM](https://ice-wm.org/)-themes in 96 and 192dpi
* Optionally: [GTK+ >=3.20](https://gitlab.com/sixsixfive/ceres-gtk) themes.
* Optionally: [GTK+ <=2.24](https://gitlab.com/sixsixfive/ceres-gtk-old) themes.

#### Dependencies:

* [Noto font](https://www.google.com/get/noto/)
* IceWM theme needs PNG/imlib support!

### Howto install/update?

#### Devuan/Debian (exporting the path is a [new Frankenstein thing](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=918754) of Debian >=10)

```
su -c 'PATH=/usr/sbin:/sbin:/usr/local/sbin:$PATH apt install fakeroot git libfile-fcntllock-perl debhelper --no-install-recommends'
cd /tmp && git clone --depth 1 https://gitlab.com/sixsixfive/ceres-icewm.git
cd ceres-icewm/packaging
sh build_deb.sh
su -c 'PATH=/usr/sbin:/sbin:/usr/local/sbin:$PATH dpkg -P ceres-theme-icewm;dpkg -i ceres-theme-icewm_*.deb;apt install -f --no-install-recommends'
```

##### with sudo installed(eg: LMDE or *buntu)

```
sudo apt install fakeroot git libfile-fcntllock-perl debhelper --no-install-recommends
cd /tmp && git clone --depth 1 https://gitlab.com/sixsixfive/ceres-icewm.git
cd ceres-icewm/packaging
sh build_deb.sh
sudo sh -c 'dpkg -P ceres-theme-icewm;dpkg -i ceres-theme-icewm_*.deb;apt install -f --no-install-recommends'
```

#### SuSE or Gecko Linux

```
sudo zypper install fakeroot git rpmbuild
cd /tmp && git clone depth -1 https://gitlab.com/sixsixfive/ceres-icewm.git
cd ceres-icewm/packaging
sh build_rpm.sh
sudo zypper install --no-recommends ceres-theme-icewm*.rpm
```

#### Any other
* 1: Download and extract the theme
```
cd /tmp && curl -L https://gitlab.com/sixsixfive/ceres-icewm/-/archive/master/ceres-icewm-master.tar.gz |tar zxf -
```
* 2: Copy the content of 'ceres-icewm' to your IceWM theme dir (usually $SYSPREFIX/share/icewm/themes)
```
cp -R ceres-icewm-master/ceres-icewm/* /usr/local/share/icewm/themes
```

### FAQ

#### Window borders are too thin to resize the windows?

Just use ALT+Mouse2 to resize windows.

#### Fonts of the HiDPI theme are too small?

You need to run the XServer in 192dpi eg:

```
printf "Xft.dpi: 192\n" >>~/.Xresources
```

*if you want to change it back just remove or replace 192 wth 96!*

__Note:__ *like always GNOME/GTK ignores standards and won't scale this 
way so you have to add GDK_SCALE=2 and GDK_DPI_SCALE=0.5 to your environment to make GTK3 scale!*
